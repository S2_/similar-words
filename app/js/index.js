if (typeof(sw) == 'undefined') {
	var sw = {};
	sw.apiPrefix = '/'; 
}


(function($) {
	
	
	//helper functions	
	var shuffle = function(myArray) {
		var i = myArray.length, j, tempi, tempj;
		if (i === 0)
			return myArray;
		while (--i) {
			j = Math.floor(Math.random() * (i + 1));
			tempi = myArray[i];
			tempj = myArray[j];
			myArray[i] = tempj;
			myArray[j] = tempi;
		}
		
		return myArray;
	};
	
	var getKey = function(obj) {
		var word = [];
		for(var k in obj) word.push(k);
		return word[0];
	};
	
	var selectWordsForGame = function() {
		//select a random word from the list
		var words = sw.words[Math.round(Math.random() * sw.words.length)];
		
		var word = getKey(words);
		var similar = shuffle(words[word]);

		similar = similar.slice(0, 3);
		similar.push(word);
		similar = shuffle(similar);
		
		return {word: word, similar: similar};
	};
	
	var startGame = function(word) {
		if(typeof(word) == 'undefined') {
			word = selectWordsForGame();
		}
		sw.currentWord = word;
		$("#main").empty().html('views/index.ejs', word);
		$('.currentword').animate({
			opacity : 0
		}, 1000, 'linear');
	};
	
	var gameOver = function() {
		var result = 'AWESOME!';

		if (sw.finaltime > 33) {
			result = 'You can do better!';
		};
		if (sw.finaltime > 36) {
			result = 'meh...';
		};
		if (sw.wrongWords > 0 && sw.finaltime <= 32) {
			result = 'You are fast, but you  made errors';
		};
		
		$("#main").empty().html('views/gameover.ejs', {result: result, time: sw.finaltime, wrong: sw.wrongWords, correct: sw.correctWords});
	};
	
	var startNewGame = function() {
		var playforrounds = 20;
		sw.correctWords = 0;
		sw.rounds = 1;
		sw.wrongWords = 0;
		sw.time = Math.round((new Date()).getTime() / 1000);
		sw.gameover = false;
		
		var updateBadges = function() {
			$('.num-of-words-wrong').html(sw.wrongWords + (sw.wrongWords == 1 ? ' word' : ' words'));
			$('.num-of-words').html(sw.correctWords + (sw.correctWords == 1 ? ' word' : ' words'));
			$('.time-words').html(Math.round((new Date()).getTime() / 1000) - sw.time);
			
			if(playforrounds >= sw.rounds) {
				setTimeout(function() {
					updateBadges();
				}, 100);
			} else {
				sw.finaltime = Math.round((new Date()).getTime() / 1000) - sw.time;
				sw.gameover = true;
			}
		};
		updateBadges();
		
		startGame();
	};
	
	var loadDictionary = function(dict) {
		var w = $.ajax({
			url: sw.apiPrefix + 'words_' + dict + '.json',
			dataType: 'json'
		});
		
		
		w.done(function(words) {
			var start = $('.start'); 
			start.removeClass('disabled');
			start.html(' Start ');
			start.animate({
				opacity : 1
			}, 1000, 'linear');
			
			sw.words = words.words;
		});		
		
		w.fail(function(err) {
			console.log(err);
		});
		
		return w;
	};
	
	//events
	$(document).on('click', '.dict', function(ev) {
		var el = $(ev.currentTarget);
		var dict = el.attr('data-dict');
		$('.start-btn').html('views/start-button.ejs', {});
		loadDictionary(dict);
	});	
	
	$(document).on('click', '.start', function() {
		startNewGame();
	});

	$(document).on('click', '.word-button', function(ev) {
		var el = $(ev.currentTarget);
		
		if (el.hasClass('disabled')) {
			return;
		}
		$('.word-button').addClass('disabled');
		
		var wrongWord = undefined;
		if(sw.currentWord.word == el.html()) {
			sw.correctWords = sw.correctWords + 1;
			sw.rounds += 1;
			el.addClass('btn-success');
		} else {
			wrongWord = sw.currentWord;
			sw.wrongWords = sw.wrongWords + 1;
			el.addClass('btn-danger');
		}
		
		setTimeout(function() {
			if(sw.gameover) {
				gameOver();
			} else {
				startGame(wrongWord);
			}
		}, 300);
	});
	
	
	//globals
	sw.words = [];
	sw.correctWords = 0;
	sw.wrongWords = 0;
	sw.rounds = 0;
	sw.time = 0;
	sw.finaltime = 0;
	sw.gameover = false;
	sw.currentWord;
})(jQuery);



//utils
(function() {
	window.Utils = {};
	
	Utils.ellipsis = function(text, maxLength){
        if(typeof maxLength === 'undefined'){
            maxLength = 9000; //a large number
        }

        if (text.length <= maxLength)
            return text;

        var xMaxFit = maxLength - 1;
        var xTruncateAt = text.lastIndexOf(' ', xMaxFit);
        if (xTruncateAt == -1 || xTruncateAt < maxLength / 2)
            xTruncateAt = xMaxFit;

        return text.substr(0, xTruncateAt) + "…";
    };

})();

